import * as Fs from 'node/fs';
import * as Zlib from 'node/zlib';

import {Git} from "./git";


var git = new Git('./sample');

git.init().then(r=>{
    var promise = Promise.resolve(true);
    if(r.empty){
        promise = git.addRemote('origin','https://bitbucket.org/ecmal/sample.git').then();
    }
    return promise.then(r=>git.showRemote('origin').then(r=>{
        var release;
        for(var branch of r.branches){
            if(branch.name == 'release'){
                release = branch;
                break;
            }
        }
        if(!release){
            return git.checkout('--orphan','release').then(r=>{
                console.info(r);
            })
        }
    }))
}).catch(e=>console.info(e));
